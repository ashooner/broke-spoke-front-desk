Errors = {
  // Local (client-only) collection
  collection: new Meteor.Collection(null),
  
  throw: function(message, timeout) {
    Errors.collection.insert({message: message, seen: false, timeout:timeout})
  },
  clearSeen: function() {
    Errors.collection.remove({seen: true});
  }
};
