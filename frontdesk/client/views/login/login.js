Template.login.events({
'submit form': function(event, template){
    event.preventDefault();
    var emailVar = template.find('#login-email').value;
    var passwordVar = template.find('#login-password').value;
    Meteor.loginWithPassword(emailVar, passwordVar, function(err){
        console.log(err);
        //TODO: Change this to check for intention and/or role
        //Router.redirect('/admin');
    });
}
});