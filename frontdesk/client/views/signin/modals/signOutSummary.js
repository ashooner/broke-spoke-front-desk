Template.signOutSummary.created = function () {
    // 1. Initialization

    var instance = this;

    // initialize the reactive variables
    instance.loaded = new ReactiveVar(0);
    instance.ready = new ReactiveVar(false);

    // 2. Autorun

    // will re-run when the "limit" reactive variables changes
    instance.autorun(function () {

        // subscribe to the posts publication
        var subscription = Meteor.subscribe('singleTimelog', Session.get('signOutTimelogId'));

        // if subscription is ready, set limit to newLimit
        if (subscription.ready()) {

            instance.ready.set(true);
        } else {
            instance.ready.set(false);
        }

    });

    // 3. Cursor
    instance.timelog = function() {
        return Timelogs.findOne(Session.get('signOutTimelogId'));
    };


}

Template.signOutSummary.helpers({
    // the posts cursor
    timelog: function () {
        return Template.instance().timelog();
    },

    transaction: function(){

        var timelog = Template.instance().timelog();

        if (timelog.transactionId){
            var transactionInfo = {};
            var transaction = Transactions.findOne(timelog.transactionId);

            if (transaction.transactionType === "volunteer-credit"){
                transactionInfo.equityCredit = true;
            } else if (transaction.paymentType === "equity"){
                transactionInfo.equityCharge = true;
            } else {
                transactionInfo.cashCharge = true;
            }

            transactionInfo.amount = transaction.amount;
            return transactionInfo;
        }

    },

    // the subscription handle
    isReady: function () {
        return Template.instance().ready.get();
    },
})

Template.signOutSummary.events({



})