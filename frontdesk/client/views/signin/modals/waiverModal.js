
Template.waiverModal.helpers({

    date : function(){
        return moment().format('Do') + " day of " + moment().format('MMMM') + " " + moment().format('YYYY');
    }
})

Template.waiverModal.rendered = function(){

    $('#bootstrapModal').addClass('modal-wide');
},

Template.waiverModal.onDestroyed(function(){
    //not working
    //$('#bootstrapModal').removeClass('modal-wide');
})

Template.waiverModal.helpers({
    person: function(){
        return People.findOne(Session.get('signInId'));
    }
})

Template.waiverModal.events({
    "click .accept": function(event, template){
        //write acceptance into the person record
        People.update(
            Session.get('signInId'),
            {$set:{waiverAcceptDate: new Date()}}, {}, function(err){
                $(".modal-dialog").css("width", "");
                if (err){
                    //TODO: add error notification
                    BootstrapModal.hide();
                    $('#bootstrapModal').removeClass('modal-wide');
                    BootstrapAlert.throw(err.message,0,'danger');
                } else {
                    $('#bootstrapModal').removeClass('modal-wide');
                    BootstrapModal.show('activitySelectModal');
                }
             });

        //Send them over to the sign-in selection modal
    }


})