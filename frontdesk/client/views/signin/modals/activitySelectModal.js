
Template.activitySelectModal.onCreated(function(){

});


Template.activitySelectModal.onRendered( function(){
    var person  = People.findOne(Session.get('signInId'));
});


Template.activitySelectModal.helpers({
    // the posts cursor

    //TODO:this isn't accessible by onRendered, or by the helper. tsupwithat?
    //I think this has to do with the bootstrapModal implementation, since it
    //calls renderWithData, it is setting the data context. I don't know if the
    //template definition gets to add to that context, or if it is getting overridden.
    person: function () {
        return People.findOne(Session.get('signInId'));
    },

    isMember: function(){
        console.log('isMember?');

        if ( new Date() <  People.findOne(Session.get('signInId')).membershipExpiration){
            return true;
        }
        return false;
    }

})

Template.activitySelectModal.events({

    "click .signin": function (event, template) {

        //Helpers don't set data context, but for some reason I can't get access
        //to the 'person' helper at all from this event handler
        var person  = People.findOne(Session.get('signInId'));

        Timelogs.insert({
            personId: person._id,
            activityType: $(event.currentTarget).data('activitytype'),
            startTime: new Date()
        }, function (err, timelogId) {
            if (err) {
                BootstrapModal.hide();
                BootstrapAlert.throw(err.message, 0, 'danger')
            } else {
                //replace all the content with a signed-in message, then hide modal
                $("#bootstrapModal .modal-footer").html("")
                $("#bootstrapModal .modal-body").html(
                    //'<span class="glyphicon glyphicon-thumbs-up" aria-hidden="true"></span>'+
                    '<h2>' +
                    person.firstName + " " + person.lastName + ' is signed in for '+
                    appSettings.activityTypes.get($(event.currentTarget).data('activitytype')).label
                    +'!' +
                    '</h2>');
                setTimeout(function () {
                    BootstrapModal.hide();
                    Session.set('signInId', null);
                }, 2000)
            }
        })
    }

})