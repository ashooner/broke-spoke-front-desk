Template.signedInList.created = function () {
    // 1. Initialization

    var instance = this;

    // initialize the reactive variables
    instance.loaded = new ReactiveVar(0);
    instance.ready = new ReactiveVar(false);

    // 2. Autorun

    // will re-run when the "limit" reactive variables changes
    instance.autorun(function () {

        // subscribe to the posts publication
        var subscription = Meteor.subscribe('currentlySignedIn');

        // if subscription is ready, set limit to newLimit
        if (subscription.ready()) {

            instance.ready.set(true);
        } else {
            instance.ready.set(false);
        }

        //UI
        $('[data-toggle="tooltip"]').tooltip();
    });

    // 3. Cursor

    instance.timelogs = function() {
        if (this.data.type == 'Volunteers'){
            return Timelogs.find(
                {endTime:{$exists:false}, activityType:'volunteering'}
            )
        } else {
            return Timelogs.find(
                {endTime:{$exists:false}, activityType:{$not:'volunteering'}}
            )
        }

    };


}

Template.signedInList.helpers({
    // the posts cursor
    timelogs: function () {
        return Template.instance().timelogs();
    },


    // the subscription handle
    isReady: function () {
        return Template.instance().ready.get();
    },
})

Template.signedInList.events({

    'click .table tbody tr': function(event){
        Router.go('timelogsAdminEditTimelog', {timelogId:$(event.target).parents('tr').data("id") });
    },

    'click .signout': function(event){
        var timelog = Timelogs.findOne($(event.currentTarget).parents('tr').data("id"));

        if (timelog.activityType == 'stand-time'){
            //do a popover
            $(event.currentTarget).popover(
                {

                    title:"Select Payment",
                    //trigger:"manual",
                    placement: 'left',
                    html:true,
                    content:'<button class="btn btn-primary  payment" data-payment="equity">Equity</button>&nbsp;' +
                    '<button class="btn btn-primary payment" data-payment="cash">Cash/Credit</button>'

                });
            $(event.currentTarget).popover('show');
        } else {
            var now = new Date();
            Timelogs.update($(event.currentTarget).parents('tr').data("id"), {$set:{endTime:now}}, {}, function(err){
                if (err){
                    console.error(err.message);
                    Errors.throw(err.message, 4000);
                }
            });
        }
        event.stopPropagation();
    },

    'click .payment': function(event, template){
        var now = new Date();
        $(event.currentTarget).closest('tr').find('.signout').popover('hide');

        Timelogs.update(
            $(event.currentTarget).parents('tr').data("id"),
            {$set:{endTime:now, paymentType:$(event.currentTarget).data('payment')}},
            {},
            function(err){
                if (err){
                    BootstrapAlert(err.message, 4000, 'danger');
                }
            });

    },


    'click .signout-all': function(event, template){
        //var now = moment().format("YYYY-MM-DDT18:00");
        //get the ids of allon the list
        var ids = template.$('table tr').map(function(){return $(this).data('id')}).get();
        var now = new Date();
        Meteor.call('timelogsSignOut', ids);

    },

    'click .delete': function(event, temp) {
        Timelogs.remove($(event.target).parents('tr').data("id"));
        event.stopPropagation();
    }
})