Template.timelogsAdminNew.helpers({
    now: function () {
      return moment().format("YYYY-MM-DDT18:00");
    },

    later: function(){
        return moment().add(1, 'hours').format("YYYY-MM-DDT21:00");
    },
    
    //Name autocomplete
    settings: function(){return {
        position: 'bottom',
        limit: 10,
        rules: [
          {
            collection: 'People',
            subscription: 'autocomplete-people',
            field: 'lastName',
            matchAll: true,
            template: Template.timelogsPersonAutocomplete
          }
        ],

  }},
    people: function() {
        return People.find();
    }

});

Template.timelogsAdminNew.events({
  
    'click .clear': function(event){
      $('input[name=personId]').val('');
      $('.selectedPerson').hide();
      $('.selectedPerson .name').text('');
      $('#personAutocomplete').show();
      event.stopPropagation(); 
    },

    //show payment option if needed (right now hard-coded for stand time only)
    'change #activityType,  change #endTime': function(event){
        if ($('#activityType').val() == 'stand-time'  && $('#endTime').val() ){
            $('.payment').show()
        } else {
            $('.payment').hide();
            $('#paymentType').val('');
        }
    },

    'autocompleteselect input': function(event, template, doc){
        $('input[name=personId]').val(doc._id);
        $('.-autocomplete-container').hide();
        $(event.currentTarget).hide().val('');
        $('.selectedPerson .name').text(doc.firstName + " " + doc.lastName);
        $('.selectedPerson').show();
    }

});

Template.timelogsAdminNew.rendered = function(){

    if (this.data.person){
        var doc = this.data.person;
        $('input[name=personId]').val(doc._id);
        $('#personAutocomplete').hide().val('');
        $('.selectedPerson .name').text(doc.firstName + " " + doc.lastName);
        $('.selectedPerson .delete').hide();
        $('.selectedPerson').show();
    }
};
