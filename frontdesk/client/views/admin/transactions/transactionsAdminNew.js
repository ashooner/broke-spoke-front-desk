Template.transactionsAdminNew.helpers({
    now: function () {
      return moment().format("YYYY-MM-DDTHH:mm");
    },
      //AUTOCOMPLETE STUFF

    settings: function(){return {
        position: 'bottom',
        limit: 10,
        rules: [
          {
            // token: '',
            collection: 'People',
            subscription: 'autocomplete-people',
            field: 'lastName',
            matchAll: true,
            template: Template.timelogsPersonAutocomplete
          }
        ],

  }},
    people: function() {
        return People.find();
    }
});

Template.transactionsAdminNew.rendered = function(){

    if (this.data.person){
        var doc = this.data.person;
        $('input[name=personId]').val(doc._id);
        $('#personAutocomplete').hide().val('');
        $('.selectedPerson .name').text(doc.firstName + " " + doc.lastName);
        $('.selectedPerson .delete').hide();
        $('.selectedPerson').show();
    }
};


Template.transactionsAdminNew.events({

    'click .clear': function(){
        $('input[name=personId]').val('');
        $('.selectedPerson').hide();
        $('.selectedPerson .name').text('');
        $('#personAutocomplete').show();
    },

    'change #transactionType': function () {
        if ($("#transactionType").val() === "volunteer-credit") {
            $("#paymentType").parents('.form-group').hide();
            $('#paymentType').val('equity');
        } else {
            $("#paymentType").parents('.form-group').show();
        }
    },

    'autocompleteselect input': function(event, template, doc){
        $('input[name=personId]').val(doc._id);
        $('.-autocomplete-container').hide();
        $(event.currentTarget).hide().val('');
        $('.selectedPerson .name').text(doc.firstName + " " + doc.lastName);
        $('.selectedPerson').show();
    }


})