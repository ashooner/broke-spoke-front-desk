Template.peopleAdminEdit.rendered = function() {
    Session.set('timelogsFilter', {personId: this.data.person._id});
    Session.set('transactionsFilter', {personId: this.data.person._id});
    Session.set('personId', this.data.person._id);

    //$('.nav-tabs a[href=#'+Router.current().params.hash+']').tab('show') ;
    $('.nav-tabs').stickyTabs();
}

Template.peopleAdminEdit.helpers({
    timelogHide: {name:true},
    transactionHide: {name:true}

});

