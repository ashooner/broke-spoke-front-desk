BaseController = RouteController.extend({
  // specify stuff that every controller should have
});

AdminController = BaseController.extend({
  layoutTemplate: 'adminLayout',
  yieldTemplates: {
  'adminTopbar': {to: 'adminTopbar'},
  'adminSidebar': {to: 'adminSidebar', data: this.data},
  'adminModal' : {to: 'adminModal'}
},

  waitOn: function() {
    // return Meteor.subscribe('waitingFor');
  },
  
  
  //TODO: This should either just be hardcoded, or put in app settings
  // Also, it's probably better to have the icon attribute connected
  // directly to the collection rather than just this representation
  adminData : function(){
      data = { collections: [
            {name: "People", handle:"people", icon:"glyphicon glyphicon-user"},
            {name: "Time logs", handle:"timelogs", icon:"glyphicon glyphicon-time"},
            {name: "Transactions", handle:"transactions", icon:"glyphicon glyphicon-tint"},
            //{name: "Bikes", handle:"bikes", icon:"fa fa-bicycle"}
        ]
    };
    return data;
  },

  data: function () {
    return this.adminData();
  },

  onBeforeAction: function(){
    this.next();
    Session.set('adminList', this.params.collectionHandle);
  },

  action: function(){
    this.render();
  }
});


AdminListController = AdminController.extend({
  layoutTemplate: 'adminLayout',
  
    data: function(){
        var adminData = this.adminData();
        return adminData;
    },
    onAfterAction:function(){
      $(".sidebar li").removeClass("active"),$(".sidebar ."+Session.get("adminSection")).addClass("active")
    }
});


UI.registerHelper('transactionLabel', function(context, options) {
  if(context)
    return appSettings.transactionTypes.get(context).label;
});

UI.registerHelper('bikeStatusLabel', function(context, options) {
    if(context)
        return appSettings.bikeStatusTypes.get(context).label;
});


UI.registerHelper('formatTime', function(context, options) {
  if(context) {
    if (moment(context).isSame(Date.now(), 'day')){
      return "Today at " + moment(context).format('h:mm a');
    }
    return moment(context).format('MM/DD/YYYY, h:mm a');
  }
});

UI.registerHelper('formatTodaysTime', function(context, options) {
    if(context) {
        if (moment(context).isSame(Date.now(), 'day')){
            return moment(context).format('h:mm a');
        }
       // return moment(context).format('MM/DD/YYYY, h:mm a');
       // return moment(context).fromNow();
        return moment(context).calendar();
    }
});

UI.registerHelper('formatDate', function(context, options) {
    if(context) {
        if (moment(context).isSame(Date.now(), 'day')){
            return "Today";
        }
        return moment(context).format('MM/DD/YYYY');
    }
});

UI.registerHelper('formatDuration', function(context, options) {
        //moment.js hsa weak duration humanization...
        //get hours
            var hours =  Math.floor(context);
        //get minutes
             var minutes = context - Math.floor(context) * 60;
        return moment.duration(hours, "hours").humanize() + " " + moment.duration(minutes, "minutes").humanize();

});

UI.registerHelper('formatBalance', function(context, options) {
    if(context) {
        if (context < 0){
            return Math.abs(context);
        }
        return context;
    }
});
//
//UI.registerHelper('currencySign', function(context, options) {
//    if(context) {
//        if (context == 'equity'){
//            return "tint";
//        } else if (context == 'cash'){
//            return "usd";
//        }
//        return context;
//    }
//});


UI.registerHelper('activityLabel', function(context, options) {

  if(context){
      return appSettings.activityTypes.get(context).label;
  }

});