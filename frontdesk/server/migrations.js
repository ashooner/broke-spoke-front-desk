Migrations.add({
  version: 1,
  name: 'Creates the first user and adds it to the app-admin Role.',
  up: function() {
      var userId = Accounts.createUser({
          username: 'ashooner',
          email: 'ashooner@gmail.com',
          password: 'paster',
      });

    Roles.addUsersToRoles(userId, ['app-admin'], Roles.GLOBAL_GROUP);    
  }
});

Migrations.add({
  version: 2,
  name: 'Runs partial test import.',
  up: function() {
    Meteor.call('runImport');   
  }
});

Migrations.add({
    version: 3,
    name: 'Add shop user.',
    up: function() {
        var userId = Accounts.createUser({
            username: 'shop',
            email: 'shop@thebrokespoke.org',
            password: 'br0kesp0ke',
        });

        Roles.addUsersToRoles(userId, ['shop-admin'], Roles.GLOBAL_GROUP);
    }
});

Migrations.add({
    version: 4,
    name: 'Runs a full DB import as of 3 29 2014.',
    up: function() {
        Meteor.call('import3292015');
    }
});


