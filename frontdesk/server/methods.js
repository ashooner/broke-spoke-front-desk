Meteor.methods({
    //PEOPLE
    peopleUpdateTransactions: function (personDoc) {
        Transactions.direct.update(
            {personId: personDoc._id},
            {$set: {person: personDoc}},
            {multi: true}
        );
    },

    peopleUpdateTimelogs: function (personDoc) {
        Timelogs.direct.update(
            {personId: personDoc._id},
            {$set: {person: personDoc}},
            {multi: true}
        );
    },

    peopleRemoveTransactions: function (personDoc) {
        Transactions.direct.remove({personId: personDoc._id});
    },

    peopleRemoveTimelogs: function (personDoc) {
        Timelogs.direct.remove({personId: personDoc._id});
    },

    //TIMELOGS
    timelogsSignOut: function (ids) {
        var now = new Date();
        //Can't do multi due to limitation of collection hooks.
        //see: https://github.com/matb33/meteor-collection-hooks/issues/94
        //Timelogs.update({_id:{$in:ids}}, {$set:{endTime:now}}, {multi:true});

        _.each(ids, function (_id, i) {
            Timelogs.update(_id, {$set: {endTime: now}});
        })
    },

    //TRANSACTIONS
    transactionsSumEquity: function (person) {
        var pipeline = [
            {$match: {personId: person, paymentType: 'equity', paymentStatus: 'complete'}},
            {
                $group: {_id: null, equityTotal: {$sum: "$amount"}}
            }];
        var equityAgg = Transactions.aggregate(pipeline);

        //FIXME: this conditional could be leading to equity values getting set to 0 incorrectly
        if (equityAgg.length > 0) {
            equityTotal = equityAgg[0].equityTotal
        } else {
            equityTotal = 0;
        }

        People.update(person, {$set: {sweatEquity: equityTotal}})
    },


    //EMAIL TEST
    sendEmail: function (to, from, subject, text) {
        check([to, from, subject, text], [String]);

        // Let other method calls from the same client start running,
        // without waiting for the email sending to complete.
        this.unblock();

        Email.send({
            to: to,
            from: from,
            subject: subject,
            text: text
        });
    },





    clearCollection: function (collection) {
        collection.remove({});
    },

    import3292015: function (json) {
        console.log("Running 3-29-2015 import.");

        console.log("Removing all previous records");
        People.remove({});
        Timelogs.remove({});
        Transactions.remove({});

        console.log('Importing from json');

        var json = import3292015;
        _.each(json, function (record, i, list) {

            //Create Person record
            People.insert({
                firstName: record.firstName,
                lastName: record.lastName,
                createdOn: record.firstSeen,
                lastSignout: record.lastSeen,
                imported: true
            }, function (err, personId) {

                if (err) {
                    console.error("json import error: ", err);
                } else {
                    console.log("Record created for " + record.firstName + " " + record.lastName + ": " + personId);

                    //Add their equity balance
                    if (record.equity != 0) {
                        Transactions.insert({
                            personId: personId,
                            transactionType: 'imported',
                            amount: record.equity,
                            paymentType: 'equity',
                            paymentStatus: 'complete',
                            date: new Date(record.lastSeen)
                        }, function (err) {
                            console.error("json import error: ", err)
                        })
                    }

                    //add their last seen date
                    if (record.lastSeen) {
                        Timelogs.insert({
                            personId: personId,
                            startTime: new Date(record.lastSeen),
                            endTime: new Date(record.lastSeen),
                            activityType: 'imported'
                        }, function (err) {
                            console.error("json import error: ", err)
                        })
                    }
                }

            })
        })
    }
})


